import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';

import { PerrosComponent } from './components/perros/perros.component';
import { GatosComponent } from './components/gatos/gatos.component';
import { AvesComponent } from './components/aves/aves.component';
import { InicioComponent } from './components/inicio/inicio.component';

const rutas: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'inicio'
  },
  {
    path: 'perros',
    component: PerrosComponent
  },
  {
    path: 'gatos',
    component: GatosComponent
  },
  {
    path: 'aves',
    component: AvesComponent
  },
  {
    path: 'inicio',
    component: InicioComponent
  }

]

@NgModule({
  declarations: [
    AppComponent,
    PerrosComponent,
    GatosComponent,
    AvesComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(rutas),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
